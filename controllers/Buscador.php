<?php

declare(strict_types=1);

namespace Controllers;

use \Core\Request;

class Buscador
{
    private \Models\Buscador $modelo;

    public function __construct(\Models\Buscador $modelo)
    {
        $this->modelo = $modelo;
    }

    public function buscar(Request $request): void
    {
        $this->modelo->buscar($request->getGet('q'));
    }
}
