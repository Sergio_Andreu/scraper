<?php

declare(strict_types=1);

require './config/config.php';

spl_autoload_register(function ($clase) {
    $clase = str_replace("\\", DIRECTORY_SEPARATOR, $clase);
    require_once DIR_RAIZ . '/' . $clase . '.php';
});

$peticion = new \Core\Request($_GET, $_POST, $_SERVER);
$despachador = new \Core\Despachador();
$despachador->cargarRutas(require DIR_RAIZ . '/config/rutas.php');

$respuesta = $despachador->resolver($peticion);

echo $respuesta;

exit;