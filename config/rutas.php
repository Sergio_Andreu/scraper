<?php

$rutas = [
    # Expresión Regular URI         Modelo                   Vista                     Controlador                     Método Cont.
    ['/^\/$/',                      '\Models\Buscador',      '\Views\Buscador',        null,                           null],
    ['/^\/buscar\/$/',              '\Models\Buscador',      '\Views\Resultados',      '\Controllers\Buscador',        'buscar'],
];

return $rutas;
