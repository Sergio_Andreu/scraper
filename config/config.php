<?php

declare(strict_types=1);
error_reporting(E_ALL);

// Mostrar errores en test:
ini_set('display_errors', '1');

// Logear errores en prod, y no mostrar:
// ini_set('display_errors', 0);
// ini_set('log_errors', 1);


// CONSTANTES

// Constantes servidor
define('DIR_RAIZ', dirname(dirname(__FILE__)));
define('URL_BASE', 'http://practica.scraper.es/');
