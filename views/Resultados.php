<?php

declare(strict_types=1);

namespace Views;

class Resultados extends View
{
    private \Models\Buscador $modelo;

    public function __construct(\Models\Buscador $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        if (!empty($this->modelo->getErrores())) {

            $msj = implode('<br>', $this->modelo->getErrores());
            header("Location: " . URL_BASE . "?e=1&m=$msj");
            exit;
        } else {

            $titulo = 'Resultados';
            ob_start();
?>
            <div class="container-fluid px-4 px-lg-5">
                <div class="row align-items-center my-5">
                    <div class="col">
                        <h1 class="font-weight-light">Resultados de búsqueda para "<?= $this->modelo->getQuery() ?>"</h1>
                    </div>
                </div>
                <div class="row">
                    <?php foreach ($this->modelo->getListas() as $lista) : ?>
                        <div class="col mb-5">
                            <div class="card h-100">
                                <div class="card-body">
                                    <h2 class="card-title">
                                        <?= $lista['titulo'] ?>
                                        <a href="<?= $lista['url'] ?>">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-arrow-up-right" viewBox="0 0 16 16">
                                                <path fill-rule="evenodd" d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5z" />
                                                <path fill-rule="evenodd" d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0v-5z" />
                                            </svg>
                                        </a>
                                    </h2>
                                    <?php if (empty($lista['productos'])) : ?>
                                        <p class="card-text">No hay resultados</p>
                                    <?php else : ?>
                                        <ul class="list-group resultados">
                                            <?php foreach ($lista['productos'] as $producto) : ?>
                                                <li class="list-group-item">
                                                    <a href="<?= $producto['enlace'] ?>">
                                                        <img src="<?= $producto['imagen'] ?>" alt="imagen de <?= $producto['nombre'] ?>">
                                                        <br>
                                                        <?= $producto['nombre'] ?>
                                                    </a>
                                                    <br>
                                                    <?= $producto['precio'] ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
<?php
            $contenido = ob_get_clean();

            ob_start();
            $this->plantilla($titulo, $contenido);
            $html = ob_get_clean();

            return $html;
        }
    }
}
