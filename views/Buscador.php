<?php

declare(strict_types=1);

namespace Views;

class Buscador extends View
{
    private \Models\Buscador $modelo;

    public function __construct(\Models\Buscador $modelo)
    {
        $this->modelo = $modelo;
    }

    public function render(): string
    {
        $titulo = 'Buscador';
        ob_start();
?>
        <div class="container px-4 px-lg-5">
            <div class="row align-items-center my-5">
                <div class="col">
                    <h1 class="font-weight-light">Consultoría Montessori Entertainment</h1>
                </div>
            </div>

            <div class="row">
                <div class="col mb-5">
                    <form class="" action="<?= URL_BASE ?>buscar/" method="GET">
                        <div class="card h-100">
                            <div class="card-body">
                                <h2 class="card-title">Buscador ferreterías</h2>
                                <div class="row mb-3">
                                    <div class="col">
                                        <input type="text" name="q" class="form-control" required placeholder="¿Qué producto busca?    Ej: 'tuerca'">
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer"><button type="submit" class="btn btn-primary">Buscar</button></div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col mb-5">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title">Nuestros proveedores</h2>
                            <ul id="proveedores" class="list-group list-group-flush">
                                <li class="list-group-item">
                                    <a href="https://www.bricodepot.es/">
                                        <img src="<?= URL_BASE ?>assets/bricodepot-logo.svg" alt="Logo bricodepot.com">
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="https://www.bricolemar.com/">
                                        <img src="<?= URL_BASE ?>assets/bricolemar-logo.jpg" alt="Logo bricolemar.com">
                                    </a>
                                </li>
                                <li class="list-group-item">
                                    <a href="https://ferreteria.es/">
                                        <img src="<?= URL_BASE ?>assets/ferreteria-logo.jpg" alt="Logo ferreteria.es">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php
        $contenido = ob_get_clean();

        ob_start();
        $this->plantilla($titulo, $contenido);
        $html = ob_get_clean();

        return $html;
    }
}
