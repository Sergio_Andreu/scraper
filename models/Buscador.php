<?php

declare(strict_types=1);

namespace Models;

use \Libs\simple_html_dom;

class Buscador
{
    private bool $accion_realizada;
    private string $query;
    private array $errores;
    private array $listas;

    public function __construct()
    {
        $this->accion_realizada = false;
        $this->query = '';
        $this->errores = [];
        $this->listas = [];
    }

    public function buscar(string $query)
    {
        if (empty($query)) {
            $this->errores[] = 'No se ha podido realizar la búsqueda';
        } else {
            $this->query = $query;
            # Scrapear sitios para resultados de búsqueda
            $this->scrapBricodepot($query);
            $this->scrapBricolemar($query);
            $this->scrapFerreteria($query);
        }
    }

    private function scrapBricodepot(string $query): void
    {
        $url_busqueda = 'http://www.bricodepot.es/catalogsearch/result/?q=' . $query;

        $lista = [
            'titulo'    => 'Bricodepot.es',
            'url'       => $url_busqueda,
            'productos' => []
        ];

        $html = new simple_html_dom();
        $html->load_file($url_busqueda);

        foreach ($html->find('ul.products-grid li.item') as $item) {

            $img = $item->find('img', 0)->src;

            $a = $item->find('h2.product-name a', 0);
            $enlace = $a->href;
            $nombre = $a->title;

            $span_precio = $item->find('div.price-box span.price', 0);
            if (!is_null($span_precio)) {
                $precio = $span_precio->innertext;
            } else {
                $precio = '--';
            }

            $lista['productos'][] = [
                'imagen' => $img,
                'nombre' => $nombre,
                'enlace' => $enlace,
                'precio' => $precio
            ];
        }

        $this->listas[] = $lista;
    }

    private function scrapBricolemar(string $query): void
    {
        $url_busqueda = 'http://www.bricolemar.com/module/ambjolisearch/jolisearch?search_query=' . $query;

        $lista = [
            'titulo'    => 'Bricolemar.com',
            'url'       => $url_busqueda,
            'productos' => []
        ];

        $html = new simple_html_dom();
        $html->load_file($url_busqueda);


        foreach ($html->find('ul#product_list li.base_product div.product-container') as $item) {

            $img = $item->find('img', 0)->getAttribute('data-src');

            $a = $item->find('p[itemprop=name] a', 0);
            $enlace = $a->href;
            $nombre = $a->title;

            $span_precio = $item->find('div.content_price span.price', 0);
            if (!is_null($span_precio)) {
                $precio = $span_precio->innertext;
            } else {
                $precio = '--';
            }

            $lista['productos'][] = [
                'imagen' => $img,
                'nombre' => $nombre,
                'enlace' => $enlace,
                'precio' => $precio
            ];
        }

        $this->listas[] = $lista;
    }

    private function scrapFerreteria(string $query): void
    {
        $url_busqueda = 'http://ferreteria.es/catalogsearch/result/?q=' . $query;

        $lista = [
            'titulo'    => 'Ferreteria.es',
            'url'       => $url_busqueda,
            'productos' => []
        ];

        $html = new simple_html_dom();
        $html->load_file($url_busqueda);

        foreach ($html->find('ul.products-grid li.item div.regular') as $item) {

            $img = $item->find('img', 0)->src;

            $a = $item->find('h2.product-name a', 0);
            $enlace = $a->href;
            $nombre = $a->title;

            $span_precio = $item->find('div.price-box span.price', 0);
            if (!is_null($span_precio)) {
                $precio = $span_precio->innertext;
            } else {
                $precio = '--';
            }

            $lista['productos'][] = [
                'imagen' => $img,
                'nombre' => $nombre,
                'enlace' => $enlace,
                'precio' => $precio
            ];
        }

        $this->listas[] = $lista;
    }

    public function getAccion(): bool
    {
        return $this->accion_realizada;
    }

    public function getQuery(): string
    {
        return $this->query;
    }

    public function getErrores(): array
    {
        return $this->errores;
    }

    public function getListas(): array
    {
        return $this->listas;
    }
}
